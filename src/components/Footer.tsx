import Link from "next/link"
import styles from "./footer.module.css"


export default function Footer() {
  return (

    <div>
      <footer className="footer text-center">
        <h3>Copyright © 2022 MangaTroc, Inc. All Rights Reserved</h3>
      </footer>
    </div>
  )
}



