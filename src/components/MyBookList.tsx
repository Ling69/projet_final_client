import axios from "axios";
import { GetServerSideProps } from "next";
import { useSession } from "next-auth/react";
import Link from "next/link";
import { Book, BookCopy } from "../pages/api/entities";

interface Props {
    books: BookCopy[];

}
const MyBookList = ({ books }: Props) => {
    // const { data: session, status } = useSession()
    // const loading = status === 'loading'
    // const user = session?.user as any;

    return (
        <>
            <div className="container">
                <h2 className="display text-center mt-2"> My Book List</h2>
                <div className="row d-flex justify-content-between">
                    {books.map((bookCopy) => (
                        <div className="col-md-3 mb-2" key={bookCopy.id}>
                            <Link href={"/books/" + bookCopy.book?.id}>
                                <div className="card table-hover">
                                    <img src={bookCopy.book?.image} className="card-img-top" style={{ width: 150, height: 150 }} />
                                    <div className="card-body">
                                        <p className="card-text">{bookCopy.book?.name} </p>
                                        <p className="card-text">{bookCopy.book?.author} </p>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    ))}
                </div>
            </div>

        </>
    )

}
export default MyBookList;