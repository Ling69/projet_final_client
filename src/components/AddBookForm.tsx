
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useState } from "react";
import api from "../pages/services/Interceptor";


const AddBookForm = () => {

    const router = useRouter()

    const handleFile = (event:any) => {
        setForm({
            ...form,
            [event.target.name]:event.target.files[0]
        })
    }
   
    const { data: session } = useSession()
    const user = session?.user
    
    const initial = {
        name: '',
        author: '',
        description: '',
        image: '',
        dateOfIssue: '',
        categoryId: ''
    }
    
    const [form, setForm] = useState(initial)
    const handleChange = (event:any) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }
    const handleSubmit = async (event:any) => {
        event.preventDefault();
        // console.log(data);
        
        try {

            const formData = new FormData();
            formData.append("name", form.name);
            formData.append("author", form.author);
            formData.append("description", form.description);
            formData.append("picture", form.image);
            formData.append("dateOfIssue", form.dateOfIssue);
            formData.append("categoryId", form.categoryId);
    
            const response = await api.post('/api/book', formData);
            router.push("/books")
            console.log(response?.data);

            
        } catch (error) {
            console.log(error);
    
        }
        
    }


    return (
        <>
            {session && user?.role == 'user' &&
                <div className="flex flex-col lg:flex-row justify-around items-center border-b  border-black">
                    <h1 className="text-xl mt-5">Add Book</h1>
                    <div className="p-2 mb-5">
                        <div className="w-full max-w-xs">
                            <form className="bg-white shadow-md mt-5 rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit}>
                                <div className="flex">
                                <div className="mb-4">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">
                                        Name
                                    </label>
                                    <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 
                                    leading-tight focus:outline-none focus:shadow-outline" placeholder="Name" type="text" 
                                    onChange={handleChange} name="name" value={form.name} required/>
                                </div>
                                <div className="mb-6">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="author">
                                        Author
                                    </label>
                                    <input className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 
                                    leading-tight focus:outline-none focus:shadow-outline"placeholder="Author" type="text" 
                                    onChange={handleChange} name="author" value={form.author} required  />
                                </div>
                                <div className="mb-6">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="description">
                                        Description
                                    </label>
                                    <input className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 
                                    leading-tight focus:outline-none focus:shadow-outline"placeholder="Description" type="text" 
                                    onChange={handleChange} name="description" value={form.description}  />
                                </div>
                                <div className="mb-6">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="image">
                                        Image
                                    </label>
                                    <input className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 
                                    leading-tight focus:outline-none focus:shadow-outline"placeholder="Image" type="file" 
                                    onChange ={handleFile} name="image"  />
                                    
                                </div>
                                </div>
                                
                                <div className="mb-6">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="date">
                                        Date
                                    </label>
                                    <input className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 
                                    leading-tight focus:outline-none focus:shadow-outline" type="date" onChange={handleChange} 
                                    name="dateOfIssue" value={form.dateOfIssue} required  />
                                </div>
                                
                                <div className="mb-6">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="categoryId">
                                        CategoryId
                                    </label>
                                    <input className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 
                                    leading-tight focus:outline-none focus:shadow-outline"placeholder="Comment_id"type="number" 
                                    onChange={handleChange} name="categoryId" value={form.categoryId} required />
                                </div>
                                <div className="flex items-center justify-between">
                                     <button className="bg-[#ba9f87] text-black font-bold py-2 px-4 rounded focus:outline-none 
                                     focus:shadow-outline" >Add
                                       
                                    </button> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}


export default AddBookForm;
