import { signIn, signOut, useSession } from "next-auth/react"
import Link from "next/link"
import SearchBar from "./SearchBar"

const Navbar = () => {

  const { data: session } = useSession()
  const user = session?.user as any;
  console.log("user", user);

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark flex justify-content-between">
      <div className="container-fluid">

<SearchBar />
      <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" 
      aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
      </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="/">Home</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/books">Books</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/blog">Blog</a>
            </li>
            {/* <li className="nav-item">
              <a className="nav-link" href="/contact">Contact</a>
            </li> */}
            <li className="nav-item">
              <a className="nav-link" href="/user">Profile</a>
            </li>
          </ul>
        </div>
        
        {session &&
          <form className="d-flex">
            <button className="btn btn-outline-light" onClick={() => signOut()}>Logout</button>
          </form>}
        {!session &&
          <>
          <form className="d-flex">
            <button className="btn btn-outline-light" onClick={() => signIn()}>Login</button>
            <button className="btn btn-outline-light" type="submit"><Link href="/register">Register</Link></button>
          </form></>
        }
        </div>
      </nav>
    </>
  )
}

export default Navbar;


