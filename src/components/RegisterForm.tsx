import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.css'
import Link from 'next/link'
import { useState } from 'react'

function RegisterForm() {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
  });

  const { name, email, password } = formData;

  const onChange = (e: any) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e: any) => {
    e.preventDefault();
      //console.log(formData);
      const newUser = {
        name,
        email,
        password,
      };
      try {

        const res = await axios.post("/api/user/register", newUser);
        
        console.log(res.data);

      } catch (error) {
        console.log(error);
      }
  }
  return (
    <>

        <h2 className="sign">Sign Up</h2>
        <form className="form-horizontal" onSubmit={(e) => onSubmit(e)} action="/action_page.php">
          <div className="form-group">
            <label className="control-label col-sm-2" htmlFor="Name">Name</label>
            <div className="col-sm-10">
              <input onChange={onChange} type="text" className="form-control" name="name" placeholder="Name" value={name} required />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2" htmlFor="email">Email</label>
            <div className="col-sm-10">
              <input onChange={onChange} type="email" className="form-control" name="email" placeholder="Your email" value={email} required />
            </div>
          </div>
          <div className="form-group">
            <label className=" control-label col-sm-2">Password</label>
            <div className="col-sm-10">
            <input onChange={onChange}  type="password" className="form-control" id="password" name="password" placeholder="Password" />
            </div>
          </div>
          {/* <div className="form-group">
            <label className=" control-label col-sm-2">Avatar</label>
            <div className="col-sm-10">
            <input onChange={onChange}  type="file" className="form-control" id="avatar" />
            </div>
          </div> */}
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button type="submit" className="btn btn-dark" onClick={onSubmit} ><Link href="/">Register</Link> </button>
            </div>
          </div>
        </form>
        {/* <style jsx>{`
        .sign{
          text-align: center;
          padding-top:30px;
        
        }
        .form-horizontal {
          margin-top: 20px;
          padding: 50px;
        }

      `}</style> */}
    </>
  )
}

export default RegisterForm


