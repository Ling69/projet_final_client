import axios from "axios"

function Avatars (name:string, picture:string) {
    return(
        <>
        <div className="flex items-center">
      <img src={picture} className="w-12 h-12 rounded-full mr-4" alt={name} />
      <div className="text-xl font-bold">{name}</div>
    </div>
    </>
    )
}


// function Avatars () {

  

//   <div>
//     <h1>Avatars</h1>
//     <ul>
//       {avatars.map(avatar => {
//     return (
//       <li key={avatar._id}>
//         {avatar.name}
//       </li>
//        )
//       })}
//     </ul>
//   </div>
// }
export default Avatars 

// export async function getStaticProps() {
//   const avatars = await axios.get('http://localhost:8000/api/uploads/avatar')
//   // .then (res => res.json());
//   console.log(avatars);
  
//   return{
//     propos:{
//       avatars
//     }
//   }
// } 