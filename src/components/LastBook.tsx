
import Link from "next/link";
import { Book } from "../pages/api/entities";
// import styles from '../styles/Home.module.css'

interface IProps {
  books: Book[]
}

const LastBook =({books}: IProps) => { 
    
    return (
        
        <div className="container">
          <h2 className="text-center m-4 text-light">Decouvrir les derniers mangas</h2>
          <div className="row d-flex justify-content-between">
            {Array.isArray(books) && books.map(book => (
              <div className="col-md-3 mb-2" key={book.id}>
                <Link href={"/books/" + book.id}>
                  <div className="card table-hover">
                    <img src={book.image} className="card-img-top" width={500} height={300} />
                    <div className="card-body">
                      <p className="card-text">{book.name} </p>
                      <p className="card-text">{book.author} </p>
                    </div>
                  </div>
                </Link>
              </div>
            ))}
          </div>
          </div>

    )
  }


  
  export default LastBook