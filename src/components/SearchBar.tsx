
import { useRouter } from 'next/router';
import Image from "next/image";

// type FormData = {
//     name: '',
//     author:'',
//     category:''
// }


export const SearchBar = () => {

    const router = useRouter();
    // const [book, setBook] = useState<Book[]>([])


    const onSearch = async (event: any) => {
        router.push({pathname: "/search", query: {search : event.target[0].value}})
        event.preventDefault();
        
    }

    return (
        <>
            <form className="d-flex" onSubmit={(e)=>onSearch(e)}>
                <Image src="/images/logo.png" className="card-img" alt="" width={50} height={50} ></Image>
                <input className="form-control me-2 mx-3" type="search" placeholder="Search" aria-label="Search" />
                <button className="btn btn-outline-success" type="submit">Search</button>
            </form>
        </>
    )
}

export default SearchBar;




