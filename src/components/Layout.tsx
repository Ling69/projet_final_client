
import Navbar from "./Navbar";

import styles from '../styles/Home.module.css';
import Footer from "./Footer";

const Layout = (props: any) => {
  return (
    <>
      <Navbar />
      <div className={styles.container}>
        <main className={styles.main}>{props.children}</main>
      </div>
      <Footer />
    </>


  )
}
export default Layout