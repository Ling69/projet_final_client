
import Link from "next/link";
import { Book, BookCopy, Loan } from "../pages/api/entities";

interface Props {
    loans: Loan[];

}
const MyLoan = ({ loans }: Props) => {
    return (
        <>

            <div className="container">
                <h2 className="display text-center mt-2"> My Loan List</h2>
                <div className="row d-flex justify-content-between">
                    {loans.map((loan) => {
                        return loan.bookCopy ? <div className="col-md-3 mb-2" key={loan.id}>
                            <Link href={"/books/" + loan.bookCopy?.book?.id}>
                                <div className="card table-hover">
                                    <img src={loan.bookCopy?.book?.image} className="card-img-top" style={{ width: 150, height: 150 }} />
                                    <div className="card-body">
                                        <p className="card-text">{loan.bookCopy.book?.name} </p>
                                        <p className="card-text">{loan.bookCopy?.book?.author} </p>
                                    </div>
                                </div>
                            </Link>
                        </div> : null
                    }
                    )}
                </div>
            </div>

        </>
    )

}
export default MyLoan;