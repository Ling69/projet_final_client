import '../styles/globals.css'
import type { AppProps } from 'next/app'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.css';
import { SessionProvider } from 'next-auth/react';
import { useEffect } from 'react';
import '../styles/Home.module.css';
import Layout from '../components/Layout';


axios.defaults.baseURL = 'http://localhost:8000';

function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  //affichage de burger menu dans navbar
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap");
  }, []);
  return (

    <SessionProvider session={session}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </SessionProvider>
  )
}
export default MyApp

