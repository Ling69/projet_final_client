import axios from "axios";
import { getSession } from "next-auth/react";

const api = axios.create({
    baseURL: process.env.NEXT_PUBLIC_SERVER_URL // create une instance
})

api.interceptors.response.use((response) => {

    return response
}, (error) => {
    // console.log("error is", error);
    

})
api.interceptors.request.use(async (config: any) => {

    const session = await getSession();
    if (session && typeof window !== "undefined") {

        config.headers.authorization = 'bearer ' + session.accessToken;
    }
    return config;
});

export default api
