import api from './Interceptor';

export class AuthService {

  static async register(user: String) {
    const response = await api.post('/api/user/', user);
    return response.data;
  }

  static async login(credentials: Record<"email" | "password", string> | undefined) {
    const response = await api.post('/api/user/login', credentials);
    
    return response.data;
  }
  static async fetchUsers() {
    const response = await api.get('/api/user');
    console.log("from fetchUser");
    return response.data;
  }

  static async fetchAccount() {
    const response = await api.get('/api/user/account')
    
    if (response?.data) {
      return response.data;
    } return null
  }
}


