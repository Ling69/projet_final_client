
import type { NextPage } from 'next'
import { signIn, signOut, useSession } from "next-auth/react"



const Login: NextPage<any> = ({ users }) => {

  const { data: session } = useSession()
  const user = session?.user as any;

  return (

    <>
      <div className="">
        {session && 
           <button onClick={() => signOut()}>
           Logout</button>
        } 
        <>
          {!session &&
            <button onClick={() => signIn()}>Login</button>
          }
        </>
        )
      </div>
    </>
  )
}


export default Login








