export interface Book {
    name: string,
    author: string,
    description: string,
    image: string,
    dateOfIssue: Date,
    categoryId: number,
    category: Category,
    id: number,
    available:number
    
}

export interface BookCopy {
    available: boolean,
    bookStatus: string,
    userId: number,
    bookId: number,
    book?: Book,
    id: number, 
}

export interface User {
    role: string,
    name: string,
    email: string,
    password: string,
    avatar: string,
    id: number
}

export interface Category {
    label: string,
    id: string
}

export interface Loan {
    id: number,
    bookCopyId: number,
    borrowId: number,
    date: Date,
    bookCopy: BookCopy;
}

