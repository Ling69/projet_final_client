import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"
import GithubProvider from "next-auth/providers/github";
import { AuthService } from "../../services/AuthService";

export default NextAuth({

  secret: process.env.JWT_SECRET,

  providers: [
    CredentialsProvider({
      // The name to display on the sign in form (e.g. 'Sign in with...')
      name: "Credentials",
      // The credentials is used to generate a suitable form on the sign in page.
      // You can specify whatever fields you are expecting to be submitted.
      // e.g. domain, username, password, 2FA token, etc.
      // You can pass any HTML attribute to the <input> tag through the object.
      credentials: {
        email: { label: "Email", type: "text", placeholder: "Your email..." },
        password: { label: "Password", type: "password", placeholder: "Your Password..." }
      },
      async authorize(credentials, req) {
        // i have to provide my own login here that takes the credentials
        // submitted and returns either a object representing a user or value
        // that is false/null if the credentials are invalid.
        // e.g. return { id: 1, name: 'Bloup', email: 'bloup@example.com' }
        // Here i use the `req` object to obtain additional parameters (i.e., the request IP address as below)
        try {
          const response = await AuthService.login(credentials);

          const data = response;
          
          // If no error and we have user data, return it
          if (data.user && data.token) {            
            return data;
          }
          // Return null if user data could not be retrieved
          return null;
        } catch (error) {
          console.log(error);
          return null;
        }
      }
    }),

    GithubProvider({
      clientId: process.env.GITHUB_ID,
      clientSecret: process.env.GITHUB_SECRET
    })
  ],
  callbacks: {
    async jwt({ token, user }) {
      //persist the OAuth access_token to the token right after signin

      if (user !== undefined) {
        
        return {
          accessToken: user.token,
          user: user.user,
        }
      }
      return token;
    },
    async session({ session, token }) {
      //send properties to the client, like an access_token from a provider
      
      session.accessToken = token.accessToken;
      session.user = token.user as any;
      return Promise.resolve(session);
    },
  },
})
