import axios from "axios";
import { GetStaticProps, NextPage } from "next";
import Link from "next/link";
import { Book } from "../api/entities";


const BookList: NextPage<{ books: Book[] }> = ({ books }) => { 
  // console.log("book", books);
  
  
  return (
      <div className="container">
        <h2 className="display text-center mt-2"> Manga Listes</h2>
        <div className="row d-flex justify-content-between">
          {books.map((book) => (
            <div className="col-md-3 mb-2" key={book.id}>
              <Link href={"/books/" + book.id}>
                <div className="card table-hover">
                  <img src={book.image} className="card-img-top" style={{ width: 200, height: 200 }} />
                  <div className="card-body">
                    <p className="card-text">{book.name} </p>
                    <p className="card-text">{book.author} </p>
                  </div>
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
  )
}

export default BookList;


// This function gets called at build time on server-side.
// It won't be called on client-side, so i do direct database queries

export const getStaticProps: GetStaticProps = async () => {
  // Call an external API endpoint to get book.
  // Here I use axios to fetch my data
  const response = await axios.get('/api/book');
  return {
    // By returning { props: { books } }, the Book page component will receive `books` as a prop at build time
    props: {
      books: response.data,
    }
  }
}

