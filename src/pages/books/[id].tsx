import axios from "axios";
import { GetServerSideProps, NextPage } from "next"
import { useSession } from "next-auth/react";
import Image from "next/image";
import { useState } from "react";
import { Book, BookCopy } from "../api/entities";
import api from "../services/Interceptor";


interface Props {
    book: Book;
    bookCopy: BookCopy[];

}
const OneBook: NextPage<Props> = ({ book, bookCopy }) => {

    const { data: session } = useSession()
    const user = session?.user as any;
    console.log("user", user);

    const [show, setShow] = useState(false)!
    const [status, setStatus] = useState<string>("new")
    const [owned, setOwned] = useState(false);
    const createCopy = async () => {
        if (status && book.id) {
            const { data } = await api.post('/api/bookCopy', { bookId: book.id, bookStatus: status })
            if (data) {
                setOwned(true)
            }
        }
    }

    const addLoan = async () => {

        const loaned = await api.post('/api/loan', { bookId: book.id, user_id: user.id })
        console.log(loaned);


    }
console.log("book", book);


    return (
        <>
            <div className="row bg-dark text-info">
                <div className="col-sm-6">
                    <Image src={book.image} width={490} height={490} />
                </div>
                <div className="col-sm-6">
                    <p className="fs-1">{book.name} </p>
                    <p className="fs-3"> Author: {book.author} </p>
                    <p className="fs-5">Description: {book.description}</p>
                    <p className="fs-5">Date of Issue: {new Intl.DateTimeFormat('en-US').format(new Date(book.dateOfIssue))}</p>
                    <p className="fs-5">Category: {book.category.label}</p>
                    <div>
                        {(() => {
                            if (bookCopy.find(item => item.available)) {
                                return (<><button className="" onClick={() => { addLoan() }}>Available</button> </>)

                            }
                            else {
                                return (<><p>Not Available</p> </>)

                            }
                        })()}
                    </div>


                    <p>Number of book: {owned ? bookCopy.length + 1 : bookCopy.length}</p>
                    {!owned && <>{user && <button className="own" onClick={() => { setShow(!show) }}>{!show ? "I have this book" : "close"}</button>}
                        {show && <>
                            <select name="status" defaultValue={status} onChange={(e) => { setStatus(e.target.value) }}>
                                <option value="new" >New</option>
                                <option value="old">Old</option>
                            </select>
                            <button onClick={() => { createCopy() }} className="btn btn-success">submit</button>
                        </>}</>}
                </div>
            </div>


            <div className="avis">
                <div>
                    <label className="control-label col-sm-2" htmlFor="Name">Avis</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" name="avis" placeholder="votre avis" required />
                    </div>
                </div>
                <style jsx>{`
        .avis{
        display: flex;
        }

      `}</style>
            </div>
        </>

    );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
    try {
        const response = await axios.get("/api/book/" + context.params?.id);
        const res = await axios.get("/api/bookCopy/book/" + context.params?.id);


        return {
            // will be passed to the page component as props
            props: {
                book: response.data,
                bookCopy: res.data,
            }
        }
    } catch (e) {
        return {
            notFound: true
        }
    }
};

export default OneBook

// export const getStaticPaths: GetStaticPaths = async () => {
//     const reponse = await axios.get<Book[]>("/api/book");
//     return {
//         paths: reponse.data.map((item) => ({ params: { id: "" + item.id } })),
//         fallback: false // false or 'blocking'
//     }
// };

// export const GetStaticProps: GetStaticProps = async (context) => {
//     try {
//         const response = await axios.get("/api/book/" + context.params?.id);
//         const res = await axios.get("/api/bookCopy/book/" + context.params?.id);
//         return {
//             // will be passed to the page component as props
//             props: {
//                 book: response.data,
//                 bookCopy: res.data,
//             }
//         }
//     } catch (e) {
//         return {
//             notFound: true
//         }
//     }
// };




