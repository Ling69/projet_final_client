
import { GetServerSideProps } from "next";
import { User } from "next-auth";
import { getSession, useSession } from "next-auth/react";
import MyBookList from "../components/MyBookList";
import MyLoan from "../components/MyLoan";
import { BookCopy, Loan } from "./api/entities";
import api from "./services/Interceptor";

interface Props {
    books: BookCopy[];
    loans: Loan[];
}
const User = ({ books, loans }: Props) => {
    console.log("books", books);
    console.log(loans);
    
    const { data: session, status } = useSession()
    const loading = status === 'loading'
    const user = session?.user as any;
    if (typeof window !== 'undefined' && loading) return null;
    if (!session) {
        return <div><h1>Access Denied</h1>
            <p>
                You must be signed in to view this page
            </p>
        </div>
    }


    return (
        <>
            <div className="container">
                <div className="card mb-3" >
                    <div className="row g-0">
                        <div className="col-md-4">
                            <img src={user.avatar} className="img-fluid rounded-start" alt="..." />
                        </div>
                        <div className="col-md-8">
                            <div className="card-body">
                                <h5 className="card-title">{user.name}</h5>
                                <p className="card-text">{user.email}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <a href="/addBook" >Add Manga</a>
                </div>
                <div className="loans">
                    {loans.length && <MyLoan loans={loans} />}
                </div>
                <div className="booklist">
                    {books && <MyBookList books={books} />}
                </div>
                <style jsx>{`
        .container {
            width: 800px;
        }
        .booklist {
            margin: 5px;
        }
        `}
                </style>
            </div>

        </>
    );
}

export default User;


export const getServerSideProps: GetServerSideProps = async (context) => {
    try {
        const session = await getSession(context);
        const config = {
            headers: { "Authorization": `bearer ${session?.accessToken}` }
        };
        const response = await api.get("/api/bookCopy/user/" + session?.user.id, config);
        
    
        const res = await api.get(`/api/loan/getLoans/${session?.user.id}`, config)
        console.log(res);
        
    
        if (session) {
            return {
                props: {
                    session: session,
                    books: response.data,
                    loans: res.data
                }
            }
        } else {
            return {
                props: {},
    
            }
        }
    } catch (error) {
        console.log(error);
        return {
            props: {},
            redirect: {
                destination: "/"
            }
        }
        
    }
   

}
