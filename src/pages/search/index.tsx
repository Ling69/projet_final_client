import axios from "axios";
import { GetServerSideProps } from "next";
import { Book } from "../api/entities"


interface Props {

    books: Book[];
}
const Search = ({ books }: Props) => {
    

    return (
        <>
         {books.map((book) => (
             <div>
                  <div className="card table-hover">
                  <img src={book.image} className="card-img-top" style={{ width: 250, height: 250 }} />
                  <div className="card-body">
                    <p className="card-text">{book.name} </p>
                    <p className="card-text">{book.author} </p>
                  </div>
                </div>
     
             </div>

         ))}

        </>
    )

}


export default Search;

export const getServerSideProps: GetServerSideProps = async (context) => {
    try {
        console.log(context.query);

        if (context.query.search) {
            const response = await axios.get('http://localhost:8000/api/book?search=' + context.query.search);
            return {
                props: {
                    books: response.data,
                }
            }
        }
        return {
            props: {},
        }

    } catch (e) {
        console.log(e);

        return {
            notFound: true
        }
    }

};