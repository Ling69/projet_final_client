import 'next-auth';
import { User } from '../src/pages/api/entities'

declare module 'next-auth' {
    interface Session {
        user: User
    }
}