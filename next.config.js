/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  images:{
    domains:["localhost", "www.coyotemag-store.fr", "www.nautiljon.com", "pinkroom-main.s3.amazonaws.com" ]
  }
}
